package computerdatabase


import computerdatabase.Config
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.core.session
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder.toHttpProtocol
import io.gatling.http.response.NoResponseBody.Empty.string

import java.util.UUID
import scala.Predef.->
import scala.concurrent.duration.DurationInt


class SceCall extends Simulation with Config {

  val httpCall = toHttpProtocol(http.baseUrl(config.sceService.url))
  val nameScript = config.sceService.scriptName
  val jsonFileFeeder = jsonFile("data/scriptBody.json").circular.convert{
    case ("id", string) => UUID.randomUUID()
  }

  def runScript() = {
    feed(jsonFileFeeder)
      .exec(http(s"run ${nameScript} ")
        .post(s"/${nameScript}")
        .body(StringBody(
          """{
      "url":  "${url}",
      "id":  "${id}"
  }"""
        ))
        .check(status.is(200))
        .check(bodyString.in("OK"))
      )
  }

  //  def runScript() ={
  //    exec(http(s"run ${nameScript} ")
  //      .post(s"/${nameScript}")
  //      .body(RawFileBody("data/scriptBody.json"))
  //      .check(status.is(200))
  //      .check(bodyString.in("OK"))
  //    )
  //  }

  val sce = scenario(s"run sce script ${nameScript}").feed(jsonFileFeeder).exec(runScript())

 // setUp(sce.inject(atOnceUsers(1)).protocols(httpCall)).assertions(global.responseTime.max.lt(800))
  setUp(sce.inject(atOnceUsers(10), // 2
    rampUsers(50).during(5.seconds), // 3
    constantUsersPerSec(50).during(60.seconds), // 4
    constantUsersPerSec(50).during(60.seconds).randomized, // 5
    rampUsersPerSec(50).to(100).during(60.seconds), // 6
    rampUsersPerSec(50).to(100).during(60.seconds).randomized).protocols(httpCall))

}
