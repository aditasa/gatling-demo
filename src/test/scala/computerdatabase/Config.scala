package computerdatabase

import com.typesafe.config.{ConfigBeanFactory, ConfigFactory}

import scala.beans.BeanProperty

trait Config {
  val config = ConfigBeanFactory.create(ConfigFactory.load(), classOf[TestConfig]);
}
//
class TestConfig {
 // @BeanProperty var loadProfile: LoadProfile = null
 // @BeanProperty var pongService: PongService = null
  @BeanProperty var sceService: SceService = null

  override def toString = s"TestConfig( floRaterService=$sceService)"
}
//
//class LoadProfile {
//  @BeanProperty var paceMilliseconds = 1
//  @BeanProperty var rampUsers = 10
//  @BeanProperty var rampSeconds = 10
//  @BeanProperty var maxDurationSeconds = 20
//  @BeanProperty var expectedResponseTimeMsec = 4000
//
//  override def toString = s"LoadProfile(paceMilliseconds=$paceMilliseconds, rampUsers=$rampUsers, rampSeconds=$rampSeconds, maxDurationSeconds=$maxDurationSeconds, expectedResponseTimeMsec=$expectedResponseTimeMsec)"
//}


class SceService {
  @BeanProperty var url = "http://10.0.10.100:9020"
  @BeanProperty var scriptName = "loadTest"
}

